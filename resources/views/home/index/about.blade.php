@extends('layouts.home')

@section('title', $title)

@section('content')
<div class="col-xs-12 col-md-12 col-lg-8">
    <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12 b-breadcrumb">    
                
                <h1>   All we care about quality content.</h1>
                <br>

<p>We care about quality content. We never wanted to be a big publishing company: Our team is small, but it’s a truly wonderful team of people who really care about what they do. Passionate and dedicated. Honest and respectful. Professional but informal. Quirky and personal. Through our articles, we are committed to nourishing productivity, improving design and development skills and finessing the work-life balance. Getting better, together, by learning from each other that is the spirit that has been our mantra for all these years.</p>
<br>
<img src="/./images/home/about-1.jpg" alt=""width="750px" height= "563px">
<br>
<br>
<p>All articles are carefully curated, edited and prepared according to the high standards set in the Debug’s Publishing Policy. These guidelines are continually revised and updated to ensure that the quality of the published content is never compromised. Debug Blog is always expanding to provide more quality content for professionals working in web design.</p>
<br>
<h1>Our Mission & Vision</h1>
<img src="/./images/home/about-2.png" alt=""width="750px" height= "563px">
<br>

<br>
<p>To be the most reliable, useful, but most importantly practical articals to open source web developers and designers. <br>

understands all web requirements and dilivering full "end to end" solutions. <br>

help people who want to grow and become entrepreneurs. <br>

provide more quality content for professionals and beginners working in web development. <br>
</p>
</div>

</div>

</div>
@endsection
